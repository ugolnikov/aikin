package agent

import (
	"bufio"
	"context"
	"fmt"
	kafka "github.com/segmentio/kafka-go"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"sync"
	"time"
)

type Agent struct {
	Host string
}

type AgentTask struct {
	Agent         *Agent
	AgentResponse *http.Response
	TaskFilePath  string
}

type AikinProcedureManager struct {
	ProcedureId      string
	PluginName       string
	Jobs             int
	Tasks            []AgentTask
	ClientResponse   *http.ResponseWriter
	PathToResults    string
	PathToDataFormat string
}

// todo продумать использование форматов деления в этом месте. (Использовать ли пользовательский плагин на сервере для этого)
func (manager *AikinProcedureManager) SeparateTasks() {
	//todo разделить файл задачи на подфайлы. Установить пути к файлам подзадач в AgentTask
}

func (manager *AikinProcedureManager) SetAgents(agents []string) {
	for index := 0; index < manager.Jobs; index++ {
		manager.Tasks = append(manager.Tasks, AgentTask{Agent: &Agent{Host: agents[index]}})
	}
	fmt.Printf("Procedure %s using agents %q\n", manager.ProcedureId, manager.GetAgents())
}

func (manager *AikinProcedureManager) GetAgents() (result []string) {
	for index, _ := range manager.Tasks {
		result = append(result, manager.Tasks[index].Agent.Host)
	}
	return
}

// todo вынести метод Sender из следующих двух методов
func (manager *AikinProcedureManager) SendPluginsRequestToAgents() {
	client := &http.Client{
		Timeout: 30 * time.Second,
	}

	waitGroup := sync.WaitGroup{}
	waitGroup.Add(len(manager.Tasks))

	routine := func(index int) {
		pluginFile, err := os.Open("./plugins/" + manager.PluginName)
		if err != nil {
			fmt.Println(err)
			return
		}
		defer pluginFile.Close()

		url := fmt.Sprintf("http://%s/plugins", manager.Tasks[index].Agent.Host)

		req, err := http.NewRequestWithContext(context.Background(),
			http.MethodPost, url, pluginFile)
		if err != nil {
			panic(err)
		}
		req.Header.Add("PluginName", manager.PluginName)
		fmt.Printf("Sending plugins to agent(%s) with plugin: %s\n", manager.Tasks[index].Agent.Host, manager.PluginName)
		res, err := client.Do(req)
		if err != nil {
			panic(err)
		}
		manager.Tasks[index].AgentResponse = res
		waitGroup.Done()
	}

	for index, _ := range manager.Tasks {
		go routine(index)
	}
	waitGroup.Wait()
}

func (manager *AikinProcedureManager) SendStartRequestToAgents() {
	client := &http.Client{
		Timeout: 30 * time.Second,
	}

	waitGroup := sync.WaitGroup{}
	waitGroup.Add(len(manager.Tasks))

	routine := func(index int) {
		//todo подставлять пути к файлам подзадач
		dataFile, err := os.Open("./plugins/data/" + manager.ProcedureId)
		defer dataFile.Close()
		if err != nil {
			fmt.Println(err)
			return
		}

		url := fmt.Sprintf("http://%s/start", manager.Tasks[index].Agent.Host)

		req, err := http.NewRequestWithContext(context.Background(),
			http.MethodPost, url, dataFile) // todo добавить разделение на таски в таскменеджер
		if err != nil {
			panic(err)
		}
		req.Header.Add("PluginName", manager.PluginName)
		req.Header.Add("TaskId", manager.ProcedureId)
		fmt.Printf("Sending start to agent(%s) with plugin: %s with task:%s\n", manager.Tasks[index].Agent.Host, manager.PluginName, manager.ProcedureId)
		res, err := client.Do(req)
		if err != nil {
			panic(err)
		}
		fmt.Printf("Agent[%s] response status: %s\n", manager.Tasks[index].Agent.Host, res.Header.Get("Status"))
		manager.Tasks[index].AgentResponse = res
		waitGroup.Done()
	}

	for index, _ := range manager.Tasks {
		go routine(index)
	}
	waitGroup.Wait()
}

func (manager *AikinProcedureManager) StoreIntermediateResults() {
	for index, _ := range manager.Tasks {
		clientResponse := *manager.ClientResponse

		if manager.Tasks[index].AgentResponse == nil {
			clientResponse.WriteHeader(http.StatusInternalServerError)
			fmt.Println("Result is nil")
			return
		}

		resultFilePath := manager.PathToResults + manager.ProcedureId + "result" + strconv.Itoa(index)
		resultFile, err := os.Create(resultFilePath)
		defer resultFile.Close()
		if err != nil {
			fmt.Println("Can not save result: ", err.Error())
			clientResponse.Header().Add("Status", fmt.Sprintf("Can not save result from %s", manager.Tasks[index].Agent.Host))
			clientResponse.WriteHeader(http.StatusInternalServerError)
			defer resultFile.Close()
			return
		}
		resultFile.ReadFrom(manager.Tasks[index].AgentResponse.Body)
	}
}

func (manager *AikinProcedureManager) ConcatenateResults() {
	clientResponse := *manager.ClientResponse

	files, err := filepath.Glob(fmt.Sprintf("./plugins/results/%s*", manager.ProcedureId))
	if err != nil {
		fmt.Println("Server can not open intermediate result files: ", err.Error())
	}
	if files == nil {
		fmt.Println("Server has no one result files to client: ")
	} else {
		fmt.Printf("Files to sending: %q\n", files)
	}

	for index, file := range files {
		resultFile, err := os.Open(file)
		if err != nil {
			fmt.Println("Server can not send result to client: ", err.Error())
			clientResponse.Header().Add("Status", fmt.Sprintf("Server can not send [%d]result to client", index))
			clientResponse.WriteHeader(http.StatusInternalServerError)
			return
		}
		defer resultFile.Close()
		writer := bufio.NewWriter(clientResponse)
		writer.ReadFrom(resultFile)
	}
}

// todo вынести в этот метод логику отправки
func (manager *AikinProcedureManager) SendResult() {
	fmt.Printf("Server send result file to client.\n")
}

func KafkaConsumer() {
	brokerURL := "localhost:9092"
	topic := "test"

	consumer := kafka.NewReader(kafka.ReaderConfig{
		Brokers: []string{brokerURL},
		Topic:   topic,
		GroupID: "test-group",
	})
	defer consumer.Close()

	for {
		message, err := consumer.ReadMessage(context.Background())
		if err != nil {
			log.Fatal("failed to read message: ", err)
		}

		fmt.Printf("Message: %s\n", string(message.Value))
	}
}
