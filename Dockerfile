FROM ubuntu:22.04

COPY . /app
WORKDIR /app

RUN apt-get update && apt-get install -y ca-certificates
RUN update-ca-certificates

RUN apt-get install -y git golang
RUN go build -o app

EXPOSE 8080

CMD ["./app"]

