package main

import (
	"aikin/agent"
	"aikin/authorization"
	"aikin/configuration"
	"bufio"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
)

// todo добавить логи
// todo сделать multipart/form-data
// todo добавить проверку авторизации во всех хендлерах
// todo добавить модули для разделения задания и объединения результатов
// todo добавить модуль авторизации агента. Присвоение уникального идентификатора агенту
func pluginHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		serverLogger.Println("Wrong http-method: expected POST")
		w.WriteHeader(http.StatusMethodNotAllowed)
	}

	pluginName := r.Header.Get("PluginName")

	file, err := os.Create("./plugins/" + pluginName)
	defer file.Close()
	if err != nil {
		fmt.Println("Can not save plugin: ", err.Error())
		fmt.Fprintf(w, "Can not save plugin")
		w.WriteHeader(http.StatusInternalServerError)
	}

	file.ReadFrom(r.Body)

	var scanner = bufio.NewScanner(r.Body)
	for scanner.Scan() {
		fmt.Printf("%q\n", scanner.Text())
	}
	err = scanner.Err()
	if err != nil {
		fmt.Fprintf(w, "Bad data in request body\n")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)
	//todo возвращать эту инфу в заголовке Status. Проверить во всех компонентах
	fmt.Fprintf(w, "Plugin %s uploaded successfull\n", pluginName)
	fmt.Printf("Upload plugin %s from: %s\n", pluginName, r.Host)
}

func startHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		serverLogger.Println("Wrong http-method: expected GET")
		w.WriteHeader(http.StatusMethodNotAllowed)
	}

	pluginName := r.Header.Get("PluginName")
	taskId := r.Header.Get("TaskId")
	jobsCount := r.Header.Get("JobsCount")

	// todo добавить автоматическое создание каталога data
	taskName := pluginName + taskId
	dataFile, err := os.Create("./plugins/data/" + taskName)
	if err != nil {
		fmt.Println("Can not save data: ", err.Error())
		fmt.Fprintf(w, "Can not save data")
		w.WriteHeader(http.StatusInternalServerError)
		defer dataFile.Close()
		return
	}
	dataFile.ReadFrom(r.Body)
	dataFile.Close()

	fmt.Printf("Server: Start calculating %s with %s jobs\n", taskName, jobsCount)

	//todo сделать make-функцию
	jobs, _ := strconv.Atoi(jobsCount)
	procedureManager := agent.AikinProcedureManager{PluginName: pluginName, ProcedureId: taskName, Jobs: jobs, ClientResponse: &w, PathToResults: "./plugins/results/"}

	//todo занести следующие три метода под интерфейс пакета agent
	procedureManager.SetAgents(configuration.GetAgents())
	procedureManager.SendPluginsRequestToAgents() // todo добавить обработку ошибок и паник
	procedureManager.SendStartRequestToAgents()   // todo добавить обработку ошибок и паник
	procedureManager.StoreIntermediateResults()   // todo добавить обработку ошибок и паник
	procedureManager.ConcatenateResults()         // todo добавить обработку ошибок и паник
	procedureManager.SendResult()                 // todo добавить обработку ошибок и паник
}

// todo добавить модуль учета клиентов и агентов через БД PostgreSQL
var LOGFILE = "server.log"
var serverLogger *log.Logger

func main() {
	PORT := ":8001"
	arguments := os.Args
	if len(arguments) == 1 {
		fmt.Println("Using default port number: ", PORT)
	} else {
		PORT = ":" + arguments[1]
	}

	http.HandleFunc("/plugins", pluginHandler)
	http.HandleFunc("/start", startHandler)
	http.HandleFunc("/authorization", authorization.AuthorizationHandler)

	f, err := os.OpenFile(LOGFILE, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer f.Close()

	serverLogger = log.New(f, "customLogLineNumber ", log.LstdFlags)
	serverLogger.SetFlags(log.LstdFlags)

	go agent.KafkaConsumer()

	err = http.ListenAndServe(PORT, nil)
	if err != nil {
		fmt.Println(err)
		return
	}
}
