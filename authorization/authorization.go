package authorization

import (
	"bufio"
	"fmt"
	"net/http"
	"os"
)

const (
	Found = iota
	NotFound
)

func formatSessionLine(sessionId string, password string) string {
	return sessionId + " " + password + "\n"
}

func formatSessionString(sessionId string, password string) string {
	return sessionId + " " + password
}

// todo добавить отдельное определение соответствия логинов
func findSessionInFile(file *os.File, sessionId string, password string) (status int) {
	reader := bufio.NewReader(file)
	fileScanner := bufio.NewScanner(reader)
	fileScanner.Split(bufio.ScanLines)
	for fileScanner.Scan() {
		line := fileScanner.Text()
		if line == formatSessionString(sessionId, password) {
			return Found
		}
	}
	return NotFound
}

func registerSession(file *os.File, sessionId string, password string) {
	count, err := file.WriteString(formatSessionLine(sessionId, password))
	if err != nil {
		fmt.Printf("Register writer as %d count with error: %s\n", count, err.Error())
	}
	fmt.Printf("Register writeer as %d count\n", count)
}

// todo переделать авторизацию через файл на обращение к БД
func checkSession(file *os.File, sessionId string, password string) (status int) {
	switch findSessionInFile(file, sessionId, password) {
	case NotFound:
		fmt.Printf("Session %s is not found\n", sessionId)
		return NotFound
	case Found:
		fmt.Printf("Session found\n")
		return Found
	}
	return NotFound
}

func AuthorizationHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Serving: %s\n", r.URL.Path)
	fmt.Printf("Served: %s\n", r.Host)

	filename := "./users/users"

	sessionId := r.Header.Get("SessionId")
	password := r.Header.Get("Password")

	//todo сделать авторизацию средствами http или при помощи специальных средств авторизации
	_, err := os.Stat(filename)
	var file *os.File
	if os.IsNotExist(err) {
		file, err = os.Create(filename)
		defer file.Close()
		if err != nil {
			fmt.Println("Can not authorize: ", err.Error())
		}
	} else {
		file, err = os.OpenFile(filename, os.O_RDWR, os.ModeAppend)
		defer file.Close()
		if err != nil {
			fmt.Println("Can not authorize: ", err.Error())
		}
		if checkSession(file, sessionId, password) == NotFound {
			fmt.Printf("Creating new session %s\n", sessionId)
			registerSession(file, sessionId, password)
			fmt.Fprintf(w, "New session %s created successfull\n", sessionId)
			fmt.Printf("Session %s created successfull\n", sessionId)
		} else {
			fmt.Fprintf(w, "Session %s authorized \n", sessionId)
			fmt.Printf("Session %s authorized \n", sessionId)
		}
	}
}
