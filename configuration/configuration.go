package configuration

import (
	"fmt"
	"github.com/spf13/viper"
)

func init() {
	viper.SetConfigType("json")
	viper.SetConfigFile("./config/serverConfig.json")
	fmt.Printf("Using config: %s\n", viper.ConfigFileUsed())
	viper.ReadInConfig()
	if viper.IsSet("agents") {
		fmt.Println("Using agents: ", viper.Get("agents"))
	} else {
		fmt.Println("Condiguration key \"agents\" need, but is not set!")
	}
}

func GetAgents() []string {
	return viper.GetStringSlice("agents")
}
